const {accounts,contract} = require("@openzeppelin/test-environment");
const { BN,expectEvent,expectRevert } = require('@openzeppelin/test-helpers');
const [account01,account02] = accounts;
const expect = require('chai').expect;
const box = contract.fromArtifact('Box');

describe('Box 合约测试',function(){
    beforeEach(async function(){
        // 为每个测试用例部署一个合约
        this.contract = await box.new({from:account01});
    })

    it('测试stor方法',async function(){
        const value = new BN('9');
        await this.contract.store(value,{from:account01});
        expect(await this.contract.retrieve()).to.be.bignumber.equal(value);
    })

    it('捕获事件',async function(){
        const value = new BN('10');
        console.log(account01);
        const reciever = await this.contract.store(value,{from:account01});
        expectEvent(reciever,'ValueChanged',{newValue:value});
    })

    it('捕获错误',async function(){
        await expectRevert(
            this.contract.store(11,{from:account02}),'Ownable: caller is not the owner'
        )
    })

    it.only('测试合约调用自己',async function(){
        console.log(this.contract.address)
    })
})
