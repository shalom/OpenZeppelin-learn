const HDWalletProvider = require("@truffle/hdwallet-provider");
const Web3 = require('web3');
const {setupLoader} = require('@openzeppelin/contract-loader');
const { projectId, mnemonic } = require('../secrets.json');
const {BigNumber} = require('bignumber.js');
const contractAddrs = [
    '0x0111463A17C8E8331C245326D821a04862738362', 
    // '0xa8D13185707F52fef80D0a3a6A214B63060a5Af1',// goerli
    '0x25b432b26632a443e77403b1435662FD2d21D063',
    '0x950C2957d42405A12Ff623292e4cC9C1E6E48A11',
    '0x79CB4709d356621Ae4f2a0326f10AD8Ec4e6e962'
]
const accounts = [ 
    '0xBCcC2073ADfC46421308f62cfD9868dF00D339a8',
    '0x1be80e5ea99793dbA9a8bA7a8d1395f8D4c14BB1',
    '0xEdFF9d58D91759438464D58d61b82BCFDFB7Bc70',
    '0x8E036Ab121fe98B864b77Bafa30AB510648D1bd1',
    '0xADcc741f24D6a98d5442B1ac8041F2CC3F63B4Fa',
    '0x842691b5e7FE06b107035be62C3d09c3abC283E1',
    '0xb7Fe1d5A1Ba13bA16685b387E47b22Ce24B2ff50',
    '0x9C187668D2ea2c459EeCA867414a698aF6861229',
    '0xA9bb0725322748839fD3e617C2EF96B1c30e6C3b',
    '0xFef7043057d7790C79C230255e284499be221b4B' 
]
const partnerAccounts = [
    '0x87E12f9b95583D52ca72ED4553f38683757FB978'
]


async function main(){
    let provider = new HDWalletProvider(mnemonic, `https://rinkeby.infura.io/v3/${projectId}`);
    // let provider = new HDWalletProvider(mnemonic, `https://goerli.infura.io/v3/${projectId}`);
    const web3 = new Web3(provider);
    
    const loader = setupLoader({provider:web3}).web3;
    const accounts = await web3.eth.getAccounts();
    console.log(accounts);

    // const address = '0xe78A0F7E598Cc8b0Bb87894B0F60dD2a88d6a8Ab';
    let SHL = {};
    const n = [1000000,1000000000,1000000000000,1000000000000000];
    for(let i = 0;i<contractAddrs.length;i++){
        
        SHL = loader.fromArtifact('SHLToken13',contractAddrs[i]);
        const value = await SHL.methods.balanceOf(accounts[0]).call();
        console.log(`账户0 SHL${i}余额：`,value);
        let v = BigNumber(10000*n[i])
        try{
            await SHL.methods.transfer(partnerAccounts[0],v).send({from:accounts[0],gas:5000000,gasPrice:10e11});
        }catch(e){
            console.log(e);
        }
        const value1 = await SHL.methods.balanceOf(partnerAccounts[0]).call();
        console.log(`账户P0 SHL${i}余额：`,value1);
    }
    // const SHL6 = loader.fromArtifact('SHLToken13',contractAddrs[0]);
    // const value = await SHL6.methods.balanceOf(accounts[0]).call();
    // console.log('账户0 SHL6余额：',value);

    // const value = await box.methods.retrieve().call();
    // console.log("01 Box value is",value);

    // send a transaction
    // await SHL6.methods.transfer(accounts[1],10000000000).send({from:accounts[0],gas:5000000,gasPrice:10e11});
    // await box.methods.store(20)
    //     .send({from:accounts[0],gas:50000,gasPrice:1e6});
    // const value1 = await SHL6.methods.balanceOf(accounts[1]).call();
    // console.log('账户1 SHL6余额：',value1);

    // const value_ = await box.methods.retrieve().call();
    // console.log("02 Box value is",value_)
}

main();

// Or, alternatively pass in a zero-based address index.
// provider = new HDWalletProvider(mnemonic, "http://localhost:8545", 5);

// Or, use your own hierarchical derivation path
// provider = new HDWalletProvider(mnemonic, "http://localhost:8545", 5, 1, true, "m/44'/137'/0'/0/");


// HDWalletProvider is compatible with Web3. Use it at Web3 constructor, just like any other Web3 Provider

